
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.io.IOException;

public  class test {

    WebDriver webdriver;

    String currentDir;

    @Test
    public void submit_file() throws InterruptedException {

        String app_url = "https://www.google.com/";
        String process_name="chromedriver.exe (32 bit)";

        //Close any running chrome instances
        Runtime rt= Runtime.getRuntime();
        try{
        Process pr =rt.exec("taskKill /F /IM" + process_name );
        }catch (IOException e){
            e.printStackTrace();

        }

        //Get webdriver instance
        if (System.getProperty("user.dir").contains("target"))
            currentDir=System.getProperty("user.dir");

                    else
        currentDir=System.getProperty("user.dir")+"\\target";

        System.setProperty("webdriver.chrome.driver",currentDir+"\\test-classes\\chromedriver.exe");
        webdriver=new ChromeDriver();
        webdriver.manage().window().maximize();


        //Launch application
        webdriver.get(app_url);

        WebDriverWait wait=new WebDriverWait(webdriver, 10);
        //Read file content
        WebElement inputBox = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='q']")));
        inputBox.sendKeys("abc");

        WebElement searchButton = wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[name='btnK']")));
        searchButton.click();

        //enter and submit

        webdriver.quit();
        webdriver.close();


    }
}
